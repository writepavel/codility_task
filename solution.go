package solution

// you can also use imports, for example:

// import "os"
import (
	"math"
)

// you can write to stdout for debugging purposes, e.g.
// fmt.Println("this is a debug message")

// Solution of the task.
func Solution(A []int, B []int, K int) int {

	roadDirectionsLimit := int(math.Min(float64(len(A)), float64(1800)))

	rootIntersection := CreateDoubleOrientedGraph(A, B)

	left := 0
	right := roadDirectionsLimit
	result := right

	for left <= right {
		lengthLimit := (right-left)/2 + left
		currentCoverage := Coverage{0, 0} // check ref - by link or value
		visited := make([]bool, len(A)+1) // check if they are all false
		updateCoverage(rootIntersection, lengthLimit, &currentCoverage, visited)
		if currentCoverage.Cameras > K {
			left = lengthLimit + 1
		} else {
			result = lengthLimit
			right = lengthLimit - 1
			//return currentCoverage.MaxLostLength + 1
		}
	}
	return result
}

func updateCoverage(rootIntersection IntersectionNode,
	lengthLimit int,
	currentCoverage *Coverage,
	visited []bool) *Coverage {
	visited[rootIntersection.ID()] = true
	rootChildren := rootIntersection.Children()
	if len(rootChildren) == 0 {
		return &Coverage{Cameras: currentCoverage.Cameras,
			MaxLostLength: 0}
	}

	shortLengths := make([]int, 0)
	for _, childNode := range rootChildren {
		if visited[childNode.ID()] {
			continue
		}
		var resultCoverage Coverage = *updateCoverage(childNode, lengthLimit, currentCoverage, visited)
		if resultCoverage.MaxLostLength+1 > lengthLimit {
			currentCoverage.Cameras += 1
		} else {
			shortLengths = append(shortLengths, resultCoverage.MaxLostLength+1)
		}
	}
	if len(shortLengths) != 0 {
		firstShortLength := shortLengths[0]
		shortLengths = shortLengths[1:]
		for _, shortLength := range shortLengths {
			if firstShortLength+shortLength > lengthLimit {
				currentCoverage.Cameras += 1
				firstShortLength = int(math.Min(float64(firstShortLength),
					float64(shortLength)))
			} else {
				firstShortLength = int(math.Max(float64(firstShortLength),
					float64(shortLength)))
			}
		}
		return &Coverage{Cameras: currentCoverage.Cameras,
			MaxLostLength: firstShortLength}

	} else {
		return &Coverage{Cameras: currentCoverage.Cameras,
			MaxLostLength: 0}
	}

}

// todo: find better solution for Node type. Maybe use pointers.

// todo how to name graph?

func CreateDoubleOrientedGraph(A []int, B []int) IntersectionNode {
	nodes := make([]IntersectionNode, len(A)+1)

	for i := range nodes {
		nodes[i] = NewIntersectionNode(i)
	}

	for j := range A {
		anode := nodes[A[j]]
		bnode := nodes[B[j]]
		bnode.AddChild(anode)
		anode.AddChild(bnode)
	}
	return nodes[0]
}

/*
func findResultCoverage(rootIntersection IntersectionNode, cameras int, roadDirectionsLimit int) Coverage {

}
*/
// Coverage of map by cameras.
type Coverage struct {
	Cameras       int
	MaxLostLength int
}

func findChildrenCoverage(root IntersectionNode, LostLengthLimit int) Coverage {
	return Coverage{0, 0}
}

func isCoverageExist(root IntersectionNode, K int) bool {
	coverage := findChildrenCoverage(root, K)
	return coverage.MaxLostLength <= K
}

// write your code in Go 1.4
type IntersectionNode interface {
	ID() int
	Children() []IntersectionNode
	AddChild(IntersectionNode)
	PopFirst() IntersectionNode
}

// AddChild to node
func (n *IntersectionNodeData) AddChild(child IntersectionNode) {
	n.ChildNodes = append(n.ChildNodes, child)
	//fmt.Printf("children: %v \n", n.ChildNodes)
}

// IntersectionNodeData - graph node representiong intersection
type IntersectionNodeData struct {
	NodeID     int
	ChildNodes []IntersectionNode
}

func (n *IntersectionNodeData) Children() []IntersectionNode {
	return n.ChildNodes
}

func (n *IntersectionNodeData) PopFirst() (first IntersectionNode) {
	first = n.ChildNodes[0]
	n.ChildNodes = n.ChildNodes[1:]
	return
}

func (n *IntersectionNodeData) ID() int {
	return n.NodeID
}

func NewIntersectionNode(id int) IntersectionNode {
	var node IntersectionNode = &IntersectionNodeData{id, nil}
	return node
}
