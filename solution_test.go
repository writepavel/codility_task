package solution_test

import (
	c "camerasTask"
	"fmt"
	"testing"
)

type testDataType struct {
	A []int
	B []int
	K int
	L int
}

var testData testDataType = testDataType{
	[]int{5, 1, 0, 2, 7, 0, 6, 6, 1},
	[]int{1, 0, 7, 4, 2, 6, 8, 3, 9},
	2,
	2}

func TestCoverage(t *testing.T) {
	cov := c.Coverage{10, 0}
	if cov.Cameras != 10 {
		t.Errorf("coverage isn't created")
	}
}

func TestNodeChildrenNotNull(t *testing.T) {
	var node c.IntersectionNode = &c.IntersectionNodeData{1,
		[]c.IntersectionNode{
			&c.IntersectionNodeData{2, nil},
			&c.IntersectionNodeData{3, nil}}}
	if node.Children == nil {
		t.Errorf("Wrong node children: %v", node.Children)
	}
}

func TestAddChildNode(t *testing.T) {
	var node c.IntersectionNode = &c.IntersectionNodeData{2, nil}
	var childNode c.IntersectionNode = &c.IntersectionNodeData{4, nil}
	node.AddChild(childNode)
	fmt.Printf("Node: %v \n", node)
	fmt.Printf("Node child: %v \n", node.Children()[0])
	if node.Children()[0].ID() != 4 {
		t.Errorf("wrong child: %v", node.Children())
	}
}

func TestPopFirstChild(t *testing.T) {
	node := c.NewIntersectionNode(2)
	childNode1 := c.NewIntersectionNode(4)
	childNode2 := c.NewIntersectionNode(8)
	node.AddChild(childNode1)
	node.AddChild(childNode2)
	node.PopFirst()
	fmt.Printf("Node: %v \n", node)
	fmt.Printf("Node child: %v \n", node.Children()[0])
	if node.Children()[0].ID() != 8 {
		t.Errorf("wrong child: %v", node.Children())
	}
}

func TestCreateDoubleOrientedGraph(t *testing.T) {
	graph := c.CreateDoubleOrientedGraph(testData.A, testData.B)
	if len(graph.Children()) == 0 {
		t.Errorf("graph wasn't populated")
	}
}

func TestSolution(t *testing.T) {
	var result int = c.Solution(testData.A, testData.B, testData.K)
	if result != testData.L {
		t.Errorf("Solution is wrong. Expected %v, but was %v", testData.L, result)
	}
}
